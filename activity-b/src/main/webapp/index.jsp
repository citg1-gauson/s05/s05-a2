<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Job Finder</title>
</head>
<body>
	<div>
		<h1>Welcome to Servlet Job Finder!</h1>
		<form action="register" method="post">
			<!-- First Name -->
				<div>
				<label for="firstname">First Name</label>
				<input type="text" name="firstname" required> 
				</div>
			<!-- Last Name -->
				<div>
				<label for="lastname">Last Name</label>
				<input type="text" name="lastname" required> 
				</div>
			<!-- Phone -->
				<div>
				<label for="phone">Phone</label>
				<input type="tel" name="phone" required> 
				</div>
			<!-- Email -->
				<div>
				<label for="email">Email</label>
				<input type="email" name="email" required> 
				</div>
			<!-- Discover -->
				<fieldset>
					<legend>How did you discover the app?</legend>
					<!-- Friend -->
					<input type="radio" id="friend" name="discover_app" value="friend" required>
					<label for="friend">Friend<br></label>
					<!-- Social Media -->
					<input type="radio" id="socialmedia" name="discover_app" value="socialmedia" required>
					<label for="socialmedia">Social Media<Br></label>
					<!-- Others -->
					<input type="radio" id="others" name="discover_app" value="others" required>
					<label for="others">Others</label>
				</fieldset>
			<!-- Date of Birth -->
				<div>
					<label for="birth_date">Date of Birth</label>
					<input type="date" name="birth_date" required>
				</div>
			<!-- Employer or Applicant -->
				<div>
					<label for="usertype">Are you an employer or applicant?</label>
						<input type="text" name="usertype" required list="usertype">
						<!-- Datalist provide auto complete feature -->
						<datalist id="usertype">
							<option value="Applicant">
							<option value="Employer">
						</datalist>
				</div>
				<div>
				<label for="description">Profile Description</label>
				<textarea name="description" maxlength="500"></textarea>
				</div>
				<button>Register</button>
		</form>
	</div>
</body>
</html>