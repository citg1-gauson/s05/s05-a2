<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
</head>
<body>
		<%
			String fullName = (String)session.getAttribute("fullName");
		%>
			<h1>Welcome <%= fullName %>!</h1>
		
		<%
			String type = (String)session.getAttribute("userType");
			if(type.equals("Applicant")) {
		%>
			<p>Welcome applicant. You may now start looking for your career opportunity.</p>
		<%
			} else if(type.equals("Employer")){ 
		%>
			<p>Welcome employer. You may now start browsing applicant profiles</p>
		<%
			}
		%>
</body>
</html>