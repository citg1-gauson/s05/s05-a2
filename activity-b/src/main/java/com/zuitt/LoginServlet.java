package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void init() throws ServletException{
		System.out.println("LoginServlet has been initialized.");
	}
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws IOException {
		
		HttpSession session = req.getSession();
		
		String firstName = (String)session.getAttribute("firstname");
		String lastName = (String)session.getAttribute("lastname");
		String userType = (String)session.getAttribute("userType");
		String fullName = firstName +" "+lastName;
		
		
		session.setAttribute("fullName", fullName);
		session.setAttribute("userType", userType);
		res.sendRedirect("home.jsp");
		
	}
	public void destroy() {
		System.out.println("LoginServlet has been finalized.");
	}
}
